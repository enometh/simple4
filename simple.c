/* simple.c
 * Copyright (C) 2017  Red Hat, Inc
 * Author: Benjamin Otte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */
#include <gtk/gtk.h>

#if GTK_MAJOR_VERSION == 4
#include <gdk/x11/gdkx.h>
#else
#include <gdk/gdkx.h>
#endif
#include <setjmp.h>

typedef struct _GPollRec GPollRec;
struct _GPollRec
{
  GPollFD *fd;
  GPollRec *prev;
  GPollRec *next;
  gint priority;
};

typedef struct _GWakeup
{
  gint fds[2];
} GWakeup;


struct _GMainContext
{
  /* The following lock is used for both the list of sources
   * and the list of poll records
   */
  GMutex mutex;
  GCond cond;
  GThread *owner;
  guint owner_count;
  GSList *waiters;

  volatile gint ref_count;

  GHashTable *sources;              /* guint -> GSource */

  GPtrArray *pending_dispatches;
  gint timeout;			/* Timeout for current iteration */

  guint next_id;
  GList *source_lists;
  gint in_check_or_prepare;

  GPollRec *poll_records;
  guint n_poll_records;
  GPollFD *cached_poll_array;
  guint cached_poll_array_size;

  GWakeup *wakeup;

  GPollFD wake_up_rec;

/* Flag indicating whether the set of fd's changed during a poll */
  gboolean poll_changed;

  GPollFunc poll_func;

  gint64   time;
  gboolean time_is_fresh;
};

void
my_main_context_reset (GMainContext *context)
{
  context->in_check_or_prepare = 0;
}

// do the main_context_iteration
void
my_main_context_iteration (GMainContext *context)
{
  gboolean some_ready;
  gint max_priority, nfds, timeout;
  static int allocated_nfds = 0;
  static GPollFD *fds = NULL;

  if (!allocated_nfds)
    {
      g_assert (!fds);
      allocated_nfds = 1;
      fds = g_new (GPollFD, allocated_nfds);
    }
  if (!g_main_context_acquire (context))
    g_error ("failed to acquire context");
  g_main_context_prepare (context, &max_priority);
  while ((nfds = g_main_context_query (context, max_priority, &timeout, fds,
                                       allocated_nfds)) > allocated_nfds)
    {
      free (fds);
      allocated_nfds = nfds;
      fds = g_new (GPollFD, nfds);
    }
  some_ready = g_main_context_check (context, max_priority, fds, nfds);
  g_main_context_dispatch (context);
  g_main_context_release (context);
}


static int
prompt(const char *msg, char *fail_msg)
{
  char *answer = NULL;
  size_t size = 0;
  int r = 1;			/* default answer y */

 prompt:
  if (isatty(STDIN_FILENO)) {
    printf("%s: [y/N] ", msg);
    fflush(stdout);
    if (getline(&answer, &size, stdin) == -1) {
      r = 0;
      fprintf(stderr, "response to query not received\n");
      exit(1);
    } else {
      if (!strcasecmp(answer, "yes\n") || !strcasecmp(answer, "y\n"))
	r = 1;
      else if  (!strcasecmp(answer, "no\n") || !strcasecmp(answer, "n\n"))
	r = 0;
      else
	goto prompt;
      if (!r && fail_msg)
	fprintf(stderr, "%s\n", fail_msg);
    }
  }
  if (answer) free(answer);
  return r;
}

static void
hello (GtkButton *widget, gpointer data)
{
  g_print ("hello world from %p on %s\n", widget, gdk_display_get_name (gtk_widget_get_display GTK_WIDGET (widget)));
}

static void
quit_cb (GtkWidget *widget,
         gpointer   data)
{
  gboolean *done = data;

  *done = TRUE;

  g_main_context_wakeup (NULL);
}

gint global_x_error_handled = 0;

GdkDisplay *gdpy_close[10];
int gdpy_close_index = 0;

jmp_buf jmp_ret;
void
x_error_quitter (Display *display, XErrorEvent *event)
{
  char buf[256];

  /* Ignore BadName errors.  They can happen because of fonts
     or colors that are not defined.  */

  if (event->error_code == BadName)
    return;

  /* Note that there is no real way portable across R3/R4 to get the
     original error handler.  */

  XGetErrorText (display, event->error_code, buf, sizeof (buf));
  fprintf(stderr, "x_error_quitter on %s: error %s on protocol request %d\n",
	  DisplayString(display), buf, event->request_code);

  gdpy_close[gdpy_close_index++] = gdk_x11_lookup_xdisplay (display);

  if (getenv("LONGJMP"))
    longjmp(jmp_ret, 1);
  else {
    global_x_error_handled = 1;
    g_main_context_wakeup (NULL);
  }
}


/* This is the handler for X IO errors, always.
   It kills all frames on the display that we lost touch with.
   If that was the only one, it prints an error message and kills Emacs.  */

static  int
x_io_error_quitter (Display *display)
{
  fprintf(stderr, "Connection lost to X Server%s\n", DisplayString (display));
  gdpy_close[gdpy_close_index++] = gdk_x11_lookup_xdisplay (display);
  if (getenv("LONGJMP"))
    longjmp(jmp_ret, 2);
  else {
    global_x_error_handled = 2;
    g_main_context_wakeup (NULL);
  }
}

static int
x_error_handler (Display *display, XErrorEvent *event)
{
  fprintf(stderr, "x_error_handler %s %p\n", DisplayString(display), event);
  if ((event->error_code == BadMatch || event->error_code == BadWindow)
      /* && event->request_code == X_SetInputFocus */)
    {
      return 0;
    }
    x_error_quitter (display, event);
    return 0;
}


void
x_io_error_exit_handler (Display *display, void *user_data)
{
  fprintf(stderr, "handling X11R7 exit on X Server%s\n", DisplayString (display));
  gdpy_close[gdpy_close_index++] = gdk_x11_lookup_xdisplay (display);
  if (getenv("LONGJMP"))
    longjmp(jmp_ret, 3);
  else {
    gboolean* done = user_data;
    *done = TRUE;
    global_x_error_handled = 3;
    g_main_context_wakeup (NULL);
  }
}

void
x_set_error_handlers()
{
  XSetErrorHandler (x_error_handler);
  XSetIOErrorHandler (x_io_error_quitter);
}

void
hello_world_on_display (GdkDisplay *gdpy, gboolean *done)
{
  gdk_display_manager_set_default_display (gdk_display_manager_get (), gdpy);
  GtkWidget *window, *button;
  Display *dpy = gdk_x11_display_get_xdisplay(gdpy);
  XSetIOErrorExitHandler (dpy, x_io_error_exit_handler, done);

  window = gtk_window_new (
#if GTK_MAJOR_VERSION == 4
#else
			   GTK_WINDOW_TOPLEVEL
#endif
			   );
  gtk_window_set_title (GTK_WINDOW (window), "hello world");
  gtk_window_set_resizable (GTK_WINDOW (window), FALSE);
  g_signal_connect (window, "destroy", G_CALLBACK (quit_cb), done);

  button = gtk_button_new ();
  gtk_button_set_label (GTK_BUTTON (button), "hello world");
  gtk_widget_set_margin_top (button, 10);
  gtk_widget_set_margin_bottom (button, 10);
  gtk_widget_set_margin_start (button, 10);
  gtk_widget_set_margin_end (button, 10);
  g_signal_connect (button, "clicked", G_CALLBACK (hello), NULL);
#if GTK_MAJOR_VERSION == 4
  gtk_window_set_child (GTK_WINDOW (window), button);
#else
  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (button));
#endif
  gtk_widget_show (window);
}

gboolean
handle_stdin_input_line (GIOChannel *source, GIOCondition c, gpointer user_data)
{
  char *line = NULL;
  GError *err = NULL;
  g_io_channel_read_line (source, &line, NULL, NULL, &err);
  if (err)
    g_error ("handle_stdin_input_line: failed: %s\n", err->message ?: "");
  if (line) {
    g_message ("trying to opening display %s", (g_strstrip (line)));
    GdkDisplay *gdpy = gdk_display_open (g_strstrip (line));
    if (!gdpy)
      g_warning ("failed to open display");
    else
      hello_world_on_display (gdpy, user_data);
    g_free (line);
  }
  return TRUE;
}

int watch_id = 0;

int
runmain (int argc, char *argv[])
{
  gboolean done = FALSE;

  global_x_error_handled = 0;

  if (!gtk_init_check (
#if GTK_MAJOR_VERSION == 4
#else
		       NULL, NULL
#endif
		       )) {
    g_warning("gtk_init_check_failed: no DISPLAY?");
    return -1;
  }
  signal (SIGPIPE, SIG_DFL);

  x_set_error_handlers();


  GIOChannel *ioc = g_io_channel_unix_new (0);
  if (!watch_id) {
    watch_id =  g_io_add_watch (ioc, G_IO_IN, handle_stdin_input_line, &done);

    GdkDisplay *gdpy = gdk_display_get_default();
    if (gdpy)
      hello_world_on_display (gdpy, &done);
  }

  GMainContext* context = g_main_context_default();
  while (!done && !global_x_error_handled) {
    g_main_context_iteration (context, TRUE);
  }

  g_message("exiting runmain");
  return 0;
}

void
maybe_fix_state(int x_error_handled)
{
  fprintf(stderr, "handled xerror from %d quitter\n", x_error_handled);
  while (gdpy_close_index > 0) {
    gdpy_close_index--;
    g_message ("closing display %s", gdk_display_get_name (gdpy_close[gdpy_close_index]));
    gdk_display_close (gdpy_close[gdpy_close_index]);
  }
  //g_main_context_release
  if (getenv ("LONGJMP"))
      my_main_context_reset (g_main_context_default ());
}

int
main()
{
  if (getenv("LONGJMP")) {
    int ret;
    if ((ret = setjmp(jmp_ret)) != 0) {
      maybe_fix_state(ret);
      goto here;
    }
  }

  while (1) {
    runmain(0,0);
    if (!getenv("LONGJMP"))
      if (global_x_error_handled)
	maybe_fix_state(global_x_error_handled);
  here:
    if (!prompt("restart main loop?", "okbai"))
      break;
//    maybe_fix_state (4);	/* force reopening display */
  }
  return 0;
}

/*
gcc simple.c $(pkg-config gtk4 --cflags --libs) -lX11 -ggdb
Xephyr :1 &
LONGJMP=1 DISPLAY=:1 ./a.out
*/
