/*
 gcc -shared -fPIC -ldl -o p.so p.c -Wall -Werror $(pkg-config glib-2.0 --cflags)
 add -DDEBUGLOG=1 for some logging (log in /tmp/p.log)
 LD_PRELOAD=${PWD}/p.so emacs
 */

#define _GNU_SOURCE

#include <err.h>
#include <dlfcn.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <glib.h>
#include <X11/Xlib.h>

#ifdef DEBUGLOG
static void
debuglog(const char *fmt, ...)
{
    va_list ap;
    FILE *f;

    va_start(ap, fmt);

    f = fopen("/tmp/p.log", "a");
    if (f) {
        vfprintf(f, fmt, ap);
        fclose(f);
    }

    va_end(ap);
}
#else
#define debuglog(fmt, ...) do { } while(0)
#endif

typedef struct {
    void *a;
} GdkDisplay;

typedef struct {
    GSource gsource;
    GdkDisplay *gdkdisplay;
} GSource_with_Display;

typedef struct {
    union {
        GSource *gsource;
        GSource_with_Display *gwd;
    };
} gsource_context_t;

static gsource_context_t *gc = NULL;
static unsigned max_gc = 0, nr_gc = 0;

typedef GSource *(*real_g_source_new_t)(GSourceFuncs *source_funcs,
                                        guint struct_size);

static void *
real_g_source_new(GSourceFuncs *source_funcs, guint struct_size) {
    return ((real_g_source_new_t)dlsym(RTLD_NEXT, "g_source_new"))
        (source_funcs, struct_size);
}

GSource *
g_source_new(GSourceFuncs *source_funcs, guint struct_size) {
    GSource *gs;
    unsigned int i;

    for (i = nr_gc; i > 0;) {
        i--;
        if (gc[i].gsource->ref_count == 1) {
            debuglog("g/c GSource %p\n", gc[i].gsource);
            g_source_unref(gc[i].gsource);
            if ((i + 1) != nr_gc)
                memmove(&gc[i], &gc[i + 1],
                        (nr_gc - (i + 1)) * sizeof(gc[0]));
            nr_gc--;
        }
    }

    gs = real_g_source_new(source_funcs, struct_size);

    if (gs && struct_size >= sizeof(GSource_with_Display)) {
        g_source_ref(gs);
        if (nr_gc >= max_gc) {
            if (!max_gc)
                max_gc = 20;
            else
                max_gc *= 2;
            gc = realloc(gc, max_gc * sizeof(gc[0]));
            if (!gc)
                err(1, "realloc");
        }
        gc[nr_gc].gsource = gs;
        nr_gc++;
    }

    debuglog("new GSource %p\n", gs);

    return gs;
}

typedef Display *(*gdk_x11_display_get_xdisplay_t)(GdkDisplay *display);

static int
gdkdisplay_xdisplay_match(GdkDisplay *gdkdisplay, Display *display)
{
    gdk_x11_display_get_xdisplay_t get_display =
        (gdk_x11_display_get_xdisplay_t)
        dlsym(RTLD_NEXT, "gdk_x11_display_get_xdisplay");

    if (get_display)
        return (get_display(gdkdisplay) == display);

    return 1;
}

static jmp_buf env;

static int
my_io_eh(Display *display)
{
    longjmp(env, 1);
}

static XIOErrorHandler current_display_io_error_eh = NULL;
static Display *current_display_io_error = NULL;

typedef int (*real_XPending_t)(Display *display);

static int real_XPending(Display *display) {
    return ((real_XPending_t)dlsym(RTLD_NEXT, "XPending"))(display);
}

int
XPending(Display *display) {
    XIOErrorHandler orig_eh;
    int ret;

    orig_eh = XSetIOErrorHandler(my_io_eh);
    if (!setjmp(env))
        ret = real_XPending(display);
    else {
        int i;

        debuglog("caught a fish on Display %p\n", display);

        for (i = 0; i < nr_gc; i++) {
            if (!gc[i].gsource->name)
                continue;
            debuglog("check GSource %p Name %s\n", gc[i].gsource,
                     gc[i].gsource->name);

            if (strncmp(gc[i].gsource->name, "GDK X11 Event source",
                        strlen("GDK X11 Event source")))
                continue;

            if (!gdkdisplay_xdisplay_match(gc[i].gwd->gdkdisplay, display))
                continue;

            debuglog("closing Display %p with GSource %p\n", display,
                     gc[i].gsource);

            g_source_destroy(gc[i].gsource);

            g_source_unref(gc[i].gsource);
            if ((i + 1) != nr_gc)
                memmove(&gc[i], &gc[i + 1],
                        (nr_gc - (i + 1)) * sizeof(gc[0]));
            nr_gc--;

            current_display_io_error_eh = orig_eh;
            current_display_io_error = display;
            break;
        }
	if (orig_eh) orig_eh(display);
        ret = 0;
    }

    XSetIOErrorHandler(orig_eh);
    return ret;
}

typedef gboolean (*real_gtk_events_pending_t)(void);

static gboolean
real_gtk_events_pending(void) {
    return
        ((real_gtk_events_pending_t)dlsym(RTLD_NEXT, "gtk_events_pending"))();
}

gboolean
gtk_events_pending(void) {
    gboolean ret;

    ret = real_gtk_events_pending();

    if (current_display_io_error) {
        Display *display = current_display_io_error;
        current_display_io_error = NULL;

        debuglog("io_error_eh for Display %p\n", display);

        current_display_io_error_eh(display);
    }

    return ret;
}

/*
 * hook g_source_new
 *   + store new_source return value
 *   + keep extra ref on sources, g/c when we hold the last ref
 *
 * hook Xpending (display)
 *   + setjmp / longjmp from io error handler
 *     + if longjmp'ed search for display in stored gsources
 *     + bust flags in gsources
 *     + stash original x io error handler and display in globals
 *   + return from Xpending with 0
 *
 * hook gtk check events pending
 *   + check globals for x io error handler and display
 *     + call if set
 */
